var stateCheck = setInterval(function() {
	if (document.readyState === 'complete') {
		clearInterval(stateCheck);
		scripts();

	}
}, 100);

function hasClass(element, cls) {
    return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function scripts(){
	$header = document.querySelector('header');
	var headerHeight = outerHeight(document.querySelector('header .top_part'));

	function openMenu(e) {
		var hamburger = $header;
		hamburger.classList.toggle('hover');

		document.querySelector('body').classList.toggle('noscroll');
		document.querySelector('html').classList.toggle('noscroll');
	}

	function outerHeight(el) {
	  var height = el.offsetHeight;
	  var style = getComputedStyle(el);

	  height += parseInt(style.marginTop) + parseInt(style.marginBottom);
	  return height;
	}

	function toggleMenu(e) {
		var el = e.target;
		while (el.tagName != 'LI') {
			el = el.parentNode;
		}
		el.classList.toggle('open');
		document.querySelector('.main_menu').classList.toggle('menu-open');
	}

	function toggleMore(e) {
		var el = e.target;
		while (el.tagName != 'DIV') {
			el = el.parentNode;
		}
		el.classList.toggle('more-open');
	}

	function toggleSeemore(e) {
		var el = e.target;
		var parent = el.parentNode;
		for (var i = 0; i < parent.childNodes.length; i++) {
			if(hasClass(parent.childNodes[i], "description") || hasClass(parent.childNodes[i], "seemore"))
				parent.childNodes[i].classList.toggle('displaynone')
		}
	}

	function addEventListeners() {
		document.querySelector('nav .hamburger').addEventListener('click', openMenu);

		var menu_lis = document.querySelectorAll('.main_menu>li>span');
		for (var i = 0; i < menu_lis.length; i++) {
			menu_lis[i].addEventListener('click', toggleMenu);
		}

		var mores = document.querySelectorAll('.plus>span');
		for (var i = 0; i < mores.length; i++) {
			mores[i].addEventListener('click', toggleMore);
		}

		var seemores = document.querySelectorAll('.seemore');
		for (var i = 0; i < seemores.length; i++) {
			seemores[i].addEventListener('click', toggleSeemore);
		}

	}

	addEventListeners();

};
